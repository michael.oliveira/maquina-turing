#include <stdio.h>

#include <malloc.h>

#include <string.h>


typedef struct nodo {

char letra;

char est;

struct nodo *seguinte;

}estado;

estado *inicial, *final;

int bandeira=0;

//***************************************>> INFO <<********************

void info(void){

//clrscr();

printf(" \tMáquina de Turing \n");

printf(" \tAceita a linguagem... \n");

printf(" \tL={0* 1*} \n");

printf(" \tGRUPO Nº 2 \n\n");

getchar();

}



//***************************************>> PALABRA <<******************

void palavra(char letra, char est){

    estado *novo;

    novo=(estado*)malloc(sizeof(estado));

    novo->letra=letra;

    novo->est=est;

    novo->seguinte=NULL;

    if(inicial==NULL)

    inicial=novo;

    else final->seguinte=novo;

    final=novo;

}



//***************************************>> BORRAR <<******************

void apagar(void){

estado *temporal;

while(inicial!=NULL)

{

temporal=inicial;

inicial=inicial->seguinte;

free(temporal);

}

}



//***************************************>> VERIFICAR <<****************

int verificar(void){

    estado *temporal;

    int contadora, contadorb, contador;

    temporal=inicial;

    contadora = contadorb = contador = 0;

    while(temporal != NULL){

        if(temporal->letra=='0')contadora++;

        if(temporal->letra=='1')contadorb++;

        if(temporal->letra=='@')contador++;


        temporal=temporal->seguinte;

    }

    if(contadora >= 1 && contadorb >= 1 || contador==1) return 1;
    else return 0;

}


//***************************************>> MOSTRARGRAFO <<***********

void mostrargrafo(){

    estado *temporal, *anterior;

    temporal=inicial;

    if (temporal->letra=='0' || temporal->letra=='1') {

        //printf(" ");

        while(temporal->seguinte!=NULL) {

            anterior=temporal;

            printf(" (%c) -------------> (%c) com %c \n",temporal->est,temporal->est,temporal->letra);

            temporal=temporal->seguinte;

            if(temporal->seguinte==NULL)
                printf(" (%c) -------------> (%c) com %c \n",anterior->est,anterior->seguinte->est,final->letra);

        }

    } else{ 
        printf("(0) -------------> (0) com @ \n");
        printf("(0) -------------> (1) com 1 \n");
    }

}



//***************************************>> INICIO <<*********************

int main(void)

{

char vector[30], opc;

int op, i;

do

{

//clrscr();

    printf(" ========================================================== \n");

    printf(" \t1 - Informacoes do programa\n");

    printf("\n");

    printf(" \t2 - Digitar a palavra\n");

    printf("\n");

    printf(" \t3 - Analisar\n");

    printf("\n");

    printf(" \t4 - Sair \n");

    printf(" ========================================================== \n\n");

    printf(" Escolha uma opcao: ");

    scanf("%d",&op);

    fflush (stdin);

    switch (op)

    {

    case 1:

    info(); break;



    case 2:

        if(bandeira==0)

        {

            apagar();

            printf(" Digite a palavra: \n");

            scanf("%s",vector);

            fflush(stdin);

            for(i=0;i<strlen(vector);i++)

            {

                if(vector[i]=='0') palavra(vector[i],'0');

                if(vector[i]=='1'|| vector[i]=='@') palavra(vector[i],'1');

            }

            bandeira=1;

            printf(" Palavra Carregada \n");

            getchar();

            break;

        } else {

            printf(" << Já existe uma palavra carregada >>\n");



            printf(" ¨Deseja substituir? (s/n): \n");

            opc=getchar();

            if (opc=='s') {
                bandeira=0;
                break;
            }
        }

    case 3:

    //clrscr();

    printf(" **************************************************** \n");

    printf(" ***** Maquina de turing ***** \n");

    printf(" **************************************************** ");

    if(verificar()==1)

    {

        printf(" \nPalavra: %s \n",vector);

        mostrargrafo();

        printf(" >>> PALAVRA ACEITA <<<\n");

    } else {

        printf(" \nPALAVRA: %s \n",vector);

        printf(" >>> PALAVRA NÃO ACEITA <<< \n");

        printf(" Automato aceito L={0* 1*} \n");

    }

    getchar();

    break;



    case 4: 
        printf(" Presione uma Tecla para Sair"); 
        apagar();
        getchar(); 
        op=4; 
        break;



    default:

    printf(" Opcao Incorreta - Escolha uma Opcao Valida >>> Tente novamente <<<");

    getchar();break;



    }

} while(op!=4);

}
